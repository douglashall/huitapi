package edu.harvard.huit.schedule

import grails.converters.JSON
import grails.converters.XML


class ScheduleController {

	def scheduleService
	
    def activated() {
		def activated = [entityId: params.id]
		def entitySchedule = this.scheduleService.isEntityActive(activated.entityId, new Date())
		activated.activated = entitySchedule ? true : false
		withFormat {
			json { render activated as JSON }
			xml { render activated as XML }
		}
	}
	
}
