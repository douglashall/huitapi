dataSource_icb {
	dbCreate = "none" // one of 'create', 'create-drop','update'
	dialect = "org.hibernate.dialect.OracleDialect"
	jndiName = "java:comp/env/jdbc/icbDS"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}