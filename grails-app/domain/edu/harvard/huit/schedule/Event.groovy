package edu.harvard.huit.schedule


class Event {

    String name
    String description
	Date startDate
	Date endDate
	Date createdOn
	String creatorId
	Integer type
	String active

	static mapping = {
		datasource 'icb'
		table 'event'
		version false		
		id generator:'sequence', params:[sequence:'event_id_sq']
		id column:'event_id'
	}

    static constraints = {
    }
	
}
