package edu.harvard.huit.schedule


class EventEntityScope {

    String entityId
	
	static belongsTo = [
		event : Event
	]

	static mapping = {
		datasource 'icb'
		table 'event_entity_scope'
		version false		
		id generator:'sequence', params:[sequence:'event_entity_scope_id_sq']
		id column:'event_entity_scope_id'
	}

    static constraints = {
    }
	
}
