package edu.harvard.huit.schedule

class EntitySchedule {

	String entityId
    String scheduleType
	Date createdOn
	String creatorId
	
	static belongsTo = [
		event : Event
	]

	static mapping = {
		datasource 'icb'
		table 'entity_schedule'
		version false		
		id generator:'sequence', params:[sequence:'entity_schedule_id_sq']
		id column:'entity_schedule_id'
	}

    static constraints = {
    }
	
}
