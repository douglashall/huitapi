package edu.harvard.huit.schedule

class ScheduleService {

    def isEntityActive(String entityId, Date date) {
		def entitySchedules = EntitySchedule.findAllByEntityId(entityId)
		if (entitySchedules.size() == 0) return true
		def entitySchedule = entitySchedules.find {
			def start = it.event.startDate
			def end = it.event.endDate
			(!start || date.compareTo(start) >= 0) && (!end || date.compareTo(end) <= 0)
		}
		return entitySchedule ? true : false
    }
	
}
